import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class Administrador {

    @PrimaryGeneratedColumn() id: number
    @Column() nome: string
    @Column() email: string
    @Column() senha: string
    @Column() salt: string
    @Column() criado_em: string
    @Column() deletado: boolean
    @Column({nullable: true}) deletado_em: string
}
