import { Entity, PrimaryGeneratedColumn, OneToMany, ManyToOne, Column, JoinColumn } from "typeorm";
import { Loja } from "./Loja";

@Entity()
export class Adicional {

    @PrimaryGeneratedColumn() id: number
    @Column() nome: string
    @Column() preco: number
    @Column() criado_em: string
    @Column() deletado: boolean
    @Column({nullable: true}) deletado_em: string

    @ManyToOne(type => Loja)
    @JoinColumn({name: 'fk_loja'})
    loja: Loja
}
