import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

const opt = {nullable: true}

@Entity()
export class Imagem {

    @PrimaryGeneratedColumn()
    id: number

    @Column() secure_url: string
    @Column() criado_em: string
    @Column() deletado: boolean
    @Column(opt) public_id: string
    @Column(opt)  _version: number
    @Column(opt) _signature: string
    @Column(opt) width: number
    @Column(opt) height: number
    @Column(opt) _format: string
    @Column(opt) resource_type: string
    @Column(opt) bytes: number
    @Column(opt) _type: string
    @Column(opt) etag: string
    @Column(opt) placeholder: boolean
    @Column(opt) _url: string
    @Column(opt) original_filename: string
    @Column(opt) deletado_em: string
}
