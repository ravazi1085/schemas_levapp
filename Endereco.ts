import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class Endereco {

    @PrimaryGeneratedColumn() id: number
    @Column({nullable: true}) cep: string
    @Column({nullable: true}) uf: string
    @Column({nullable: true}) cidade: string
    @Column({nullable: true}) bairro: string
    @Column({nullable: true}) rua: string
    @Column({nullable: true}) numero: string
    @Column({nullable: true}) complemento: string
}