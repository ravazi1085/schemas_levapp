import { Entity, PrimaryGeneratedColumn, JoinColumn, ManyToOne } from "typeorm";
import { Adicional } from "./Adicional";
import { Produto } from "./Produto";

@Entity()
export class ProdutoAdicional {

    @PrimaryGeneratedColumn() id: number

    @ManyToOne(type => Adicional)
    @JoinColumn({name: 'fk_adicional'})
    adicional: Adicional

    @ManyToOne(type => Produto)
    @JoinColumn({name: 'fk_produto'})
    produto: Produto
}
