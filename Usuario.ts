import { Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, Unique } from "typeorm";
import { Endereco } from "./Endereco";
import { Imagem } from "./Imagem";

const opt = { nullable: true }

@Entity()
@Unique(['email'])
export class Usuario {

    @PrimaryGeneratedColumn() id: number
    @Column({name: 'email'})  email: string
    @Column()    label_id: string
    @Column()    criado_em: string
    @Column()    deletado: boolean
    @Column()    nome:    string
    @Column()    senha: string
    @Column()    salt: string
    @Column()    email_confirmado_em: string
    @Column()    telefone: string
    @Column()    cpf: string
    @Column(opt) deletado_em: string
    @Column(opt) reset_senha_key: string

    @OneToOne(type => Endereco)
    @JoinColumn({name: 'fk_endereco'})
    endereco: Endereco

    @OneToOne(type => Imagem)
    @JoinColumn({name: 'fk_imagem'})
    foto: Imagem
}