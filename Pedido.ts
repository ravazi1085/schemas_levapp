import { Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, ManyToOne } from "typeorm";
import { Usuario } from "./Usuario";
import { Loja } from "./Loja";

@Entity()
export class Pedido {

    @PrimaryGeneratedColumn() id: number
    @Column() criado_em: string
    @Column() deletado: boolean

    @ManyToOne(type => Usuario)
    @JoinColumn({name: 'fk_usuario'})
    usuario: Usuario

    @ManyToOne(type => Loja)
    @JoinColumn({name: 'fk_loja'})
    loja: Loja

    @Column() forma_pagamento: string
    @Column() pedido_obj: string
    @Column() situacao: string

}
