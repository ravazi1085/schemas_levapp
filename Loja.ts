import { Entity, PrimaryGeneratedColumn, Column, JoinColumn, OneToOne, Unique, OneToMany } from "typeorm";
import { Imagem } from "./Imagem";
import { Endereco } from "./Endereco";

const opt = {nullable: true}

@Entity()
@Unique(['email'])
export class Loja {

    @PrimaryGeneratedColumn() id: number
    @Column({name: 'email'}) email: string
    @Column()    label_id: string
    @Column()    nome_fantasia: string
    @Column()    categoria: string
    @Column()    senha: string
    @Column()    salt: string
    @Column()    status_acesso: string
    @Column()    criado_em: string
    @Column()    deletado: boolean
    @Column()    categorias_produto: string
    @Column()    telefone: string
    @Column(opt) taxa_entrega: number
    @Column(opt) contrato_assinado_em: string
    @Column(opt) agenciado_por: number
    @Column(opt) agenciado_em: string
    @Column(opt) aceito_em: string
    @Column(opt) contrato_url: string
    @Column(opt) deletado_em: Date
    @Column(opt) cnpj: string
    @Column(opt) cpf: string
    @Column(opt) foto_url: string
    @Column(opt) reset_senha_key: string

    @OneToOne(type => Imagem)
    @JoinColumn({name: 'fk_imagem'})
    foto: Imagem

    @OneToOne(type => Endereco)
    @JoinColumn({name: 'fk_endereco'})
    endereco: Endereco
    
}
