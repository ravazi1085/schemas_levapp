import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

const opt = {nullable: true}

@Entity()
export class Entregador {

    @PrimaryGeneratedColumn() id: number
    @Column() label_id: string
    @Column() nome: string
    @Column() email: string
    @Column() senha: string
    @Column() salt: string
    @Column() cpf: string
    @Column() rg: string
    @Column() data_nasc: string
    @Column() telefone: string
    @Column() veiculo: string

    @Column(opt) img_mei: string
    @Column(opt) img_habilitacao: string
    @Column(opt) img_contrato: string
    @Column(opt) img_endereco: string
    @Column(opt) img_antecedente: string
    @Column(opt) img_doc_veiculo: string

    @Column() criado_em: string
    @Column() deletado: boolean
    @Column(opt) deletado_em: string

}
