import { Entity, PrimaryGeneratedColumn, Column, JoinColumn, ManyToOne } from "typeorm";
import { Produto } from "./Produto";
import { Pedido } from "./Pedido";

@Entity()
export class ProdutoPedido {

    @PrimaryGeneratedColumn() id: number
    
    @ManyToOne(type => Produto)
    @JoinColumn({name: 'fk_produto'})
    produto: Produto

    @ManyToOne(type => Pedido)
    @JoinColumn({name: 'fk_pedido'})
    pedido: Pedido

    @Column() quantidade: number
    // json stringify de adicionais
    @Column() adicionais: string
    @Column({nullable: true}) comentario: string
}
