import { Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, ManyToOne } from "typeorm";
import { Loja } from "./Loja";
import { Imagem } from "./Imagem";

@Entity()
export class Produto {

    @PrimaryGeneratedColumn() id: number
    @Column() nome: string
    @Column() descricao: string
    @Column() preco: number
    @Column() categoria: string
    @Column() criado_em: string
    @Column() deletado: boolean
    @Column() label_id: string
    @Column() disponivel: true
    @Column({nullable: true}) escolhas: string
    @Column({nullable: true}) deletado_em: string
    @Column({nullable: true}) disponibilidade_alterada_em: string
    @Column({nullable: true}) preco_custo: number

    @ManyToOne(type => Loja)
    @JoinColumn({name: 'fk_loja'})
    loja: Loja

    @OneToOne(type => Imagem)
    @JoinColumn({name: 'fk_imagem'})
    imagem: Imagem

}
